# Arch Install Script

Just a simple bash script wizard to install Arch Linux after you have booted on the official Arch Linux install media.

With this script, you can install Arch Linux with two simple terminal commands.

This wizard is made to install minimum packages (Base, bootloader and optionally my rice).

> This script is a modification of [archfi](https://github.com/MatMoul/archfi) to fit my needs.

## How to use

First, boot with the [last Arch Linux image](https://www.archlinux.org/download/) with a [bootable device](https://wiki.archlinux.org/index.php/USB_flash_installation_media).

Then make sure you have Internet connection on the Arch iso. If you have a wireless connection the `wifi-menu` command might be useful to you. You can also read the [Network configuration](https://wiki.archlinux.org/index.php/Network_configuration) from the Arch Linux guide for more detailed instructions.

Then download the script with from the command line:
```
    curl -LO https://gitlab.com/p4r17yb17/archis/-/raw/master/archis
```
Finally, launch the script:
```
    sh archis
```
Then follow the on-screen instructions to completion.


## More custom install
```
    sh archis -cpl {URL of your custom package list}
```
You can find a sample custom package list file in the samples folder.
